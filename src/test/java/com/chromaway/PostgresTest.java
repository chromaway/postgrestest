package com.chromaway;

import org.junit.jupiter.api.Test;
import org.postgresql.util.PSQLException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class PostgresTest {
    static final String DB_URL = ""; // TODO set DB URL here
    static final String DB_USERNAME = ""; // TODO set DB user here
    static final String DB_PASSWORD = ""; // TODO set DB password here

    @Test
    void testSelectDecimalBug() throws SQLException {
        BigDecimal limit = new BigDecimal(BigInteger.TEN.pow(131072).subtract(BigInteger.ONE));

        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD)) {
            try (Statement statement = connection.createStatement()) {
                statement.execute("DROP TABLE IF EXISTS BigDecimalTest");
                statement.execute("CREATE TABLE BigDecimalTest(v DECIMAL NOT NULL);");
            }

            try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO BigDecimalTest (v) VALUES (?);")) {
                preparedStatement.setBigDecimal(1, limit);
                preparedStatement.execute();
            }

            for (int i = 0; i < 100; i++) {
                System.out.println("i = " + i);
                try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT v FROM BigDecimalTest;")) {
                    try (ResultSet resultSet = preparedStatement.executeQuery()) {
                        assertTrue(resultSet.next());
                        assertEquals(limit, resultSet.getBigDecimal(1));
                        assertFalse(resultSet.next());
                    }
                }
            }
        }
    }

    @Test
    void testSelectIntegerOverflow() throws SQLException {
        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD)) {
            try (Statement statement = connection.createStatement()) {
                statement.execute("DROP TABLE IF EXISTS IntegerOverflowTest;");
                statement.execute("CREATE TABLE IntegerOverflowTest (a BIGINT NOT NULL);");
                statement.execute("INSERT INTO IntegerOverflowTest (a) VALUES (9223372036854775807);");
                statement.execute("INSERT INTO IntegerOverflowTest (a) VALUES (1);");
            }

            for (int i = 0; i < 100; i++) {
                System.out.println("i = " + i);
                try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT SUM(a) FROM IntegerOverflowTest;")) {
                    try (ResultSet resultSet = preparedStatement.executeQuery()) {
                        assertTrue(resultSet.next());
                        assertThrows(PSQLException.class, () -> resultSet.getLong(1));
                        assertFalse(resultSet.next());
                    }
                }
            }
        }
    }
}
