# Postgres bug

This project demonstrate bugs in the Postgres JDBC driver version 42.5.1 (and possibly some earlier versions). 
These problems did not exist in version 42.2.11.

Set URL, username and password to a Postgres database in `PostgresTest.java` before running.
